package com.rancedxk.monitor.device.provider;

import java.util.Collection;

import com.rancedxk.monitor.device.Device;

/**
 * 设备管理接口
 * @author duxikun
 */
public interface IDeviceProvider {
	public boolean isContain(String code);
	
	public Device get(String code);
	
	public Collection<Device> getAll();
}
