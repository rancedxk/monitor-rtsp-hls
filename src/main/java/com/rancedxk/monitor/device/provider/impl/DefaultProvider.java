package com.rancedxk.monitor.device.provider.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.jfinal.kit.Prop;
import com.rancedxk.monitor.device.Device;
import com.rancedxk.monitor.device.provider.IDeviceProvider;

public class DefaultProvider implements IDeviceProvider{
	
	static Map<String,Device> devs = null;
	
	static{
		//读取监控设备配置信息
		Prop prop = null;
		try {
			prop = new Prop("monitor.properties");
		} catch (Exception e) {
			//加载失败时，抛出异常
			throw new RuntimeException("monitor.properties不存在，无法加载设备信息");
		}
		//读取配置信息
		devs = new HashMap<String, Device>();
		String codes = prop.get("monitor.codes");
		for(String code : codes.split(",")){
			devs.put(code, new Device(code,prop.get(String.format("monitor.%s.title",code)),prop.get(String.format("monitor.%s.stream_url",code))));
		}
	}
	
	public boolean isContain(String code){
		return devs.containsKey(code);
	}
	
	public Device get(String code){
		return devs.get(code);
	}
	
	public Collection<Device> getAll(){
		return devs.values();
	}
}
