package com.rancedxk.monitor.device;

public class Device {
	String code;
	String title;
	String streamUrl;
	
	public Device() {
	}
	
	public Device(String code,String title,String streamUrl) {
		this.code = code;
		this.title = title;
		this.streamUrl = streamUrl;
	}
	
	public void setCode(String code) {
		this.code = code;
	}
	public String getCode() {
		return code;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getTitle() {
		return title;
	}
	public void setStreamUrl(String streamUrl) {
		this.streamUrl = streamUrl;
	}
	public String getStreamUrl() {
		return streamUrl;
	}
}
