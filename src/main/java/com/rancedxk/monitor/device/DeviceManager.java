package com.rancedxk.monitor.device;

import java.util.Collection;

import com.rancedxk.monitor.device.provider.IDeviceProvider;
import com.rancedxk.monitor.task.TaskManager;

public class DeviceManager {
	static IDeviceProvider provider;
	
	public static void setProvider(IDeviceProvider provider) {
		DeviceManager.provider = provider;
		//初始化TaskManager
		TaskManager.init(provider.getAll().size());
	}
	
	public static boolean isContain(String code) {
		return provider.isContain(code);
	}
	
	public static Device get(String code) {
		return provider.get(code);
	}
	
	public static Collection<Device> getAll() {
		return provider.getAll();
	}
}
