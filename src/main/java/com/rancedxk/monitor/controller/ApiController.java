package com.rancedxk.monitor.controller;

import com.jfinal.core.Controller;
import com.rancedxk.monitor.device.DeviceManager;

import cn.hutool.json.JSONUtil;

/**
 * 对外提供接口服务
 * @author duxikun
 */
public class ApiController extends Controller{
    
    /**
     * 返回当前所有设备信息
     */
    public void getAllDevices(){
    	renderText(JSONUtil.toJsonStr(DeviceManager.getAll()));
    }
}
