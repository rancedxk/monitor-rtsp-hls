-- ----------------------------
-- Table structure for t_device
-- ----------------------------
DROP TABLE IF EXISTS `t_device`;
CREATE TABLE `t_device` (
  `code` varchar(10) NOT NULL,
  `title` varchar(20) DEFAULT NULL,
  `streamUrl` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='监控设备信息';

-- ----------------------------
-- Records of t_device
-- ----------------------------
INSERT INTO `t_device` VALUES ('dev1', '胡南卫视RTMP流', 'rtmp://58.200.131.2:1935/livetv/hunantv');
